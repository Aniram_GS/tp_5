import unittest
from chained_list import ChainedList


class TestLinkedList(unittest.TestCase):

    def setUp(self):
        """ Link between the files """
        self.test_list = ChainedList()

    def test_isEmpty(self):
        """ Check if the list is empty """
        self.assertIsNone(self.test_list.first_node)

    def test_addNode(self):
        """ Test check if adding node on an empty list"""
        self.test_list.add_node(3)
        self.assertIsNotNone(self.test_list)

    def test_isTheSame(self):
        """ Test check if an insertion and deletion has an impact on the list """
        list_1 = []
        list_2 = []
        self.test_list.add_node(3)
        self.test_list.add_node(30)
        self.test_list.add_node(18)
        for value in self.test_list:
            a = value.value
            list_1.append(a)
        self.test_list.delete_node(18)
        for value in self.test_list:
            b = value.value
            list_2.append(b)
        self.assertEqual(list_1, list_2)

    def test_isTheLast(self):
        """ Test check the last node"""
        self.test_list.add_node(5)
        self.test_list.add_node(24)
        for value in self.test_list:
            a = value.value
        print(a)
        self.assertEqual(a, 15)
