#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class Node:
    def __init__(self, value):
        self.value = value
        self.next = None

    
    def __str__(self):
        #return str(self.value)
        string = str(self.value)
        if self.next != None:
            string += ", " + str(self.next)
        return string

class ChainedList:
    def __init__(self):
        self.first_node = None

    def __iter__(self):
        value = self.first_node
        while value is not None:
            yield value
            value = value.next
    
    def add_node(self, value):
        """insert a new node after the node with the value == data
        Parameters
        ----------
        data : searched data
        new_node : node to insert
        """
        if self.first_node == None:
            self.first_node = Node(value)

        elif value < self.first_node.value:
            node = Node(value)
            node.next = self.first_node
            self.first_node = node


        elif value > self.first_node.value:
            node = Node(value)
            node.next = self.first_node
            second_node = self.first_node.next

            while second_node is not None and value > second_node.value:
                node.next = second_node
                second_node = second_node.next
            previous = node.next
            previous.next = node
            node.next = second_node


    def delete_node(self, value):
        """
        delete all node(s) value == data
        Parameters
        ----------
        data : searched data to delete
        """
        temp = self.first_node
        if temp is not None:
            if temp.value == value:
                self.first_node = temp.next
                return
        while temp is not None:
            if temp.value == value:
                break
            prev = temp
            temp = temp.next
        if temp == None:
            print('The value does not exist, please check the list')
            return
        prev.next = temp.next



    def __str__(self):
        if self.first_node == None:
            return "liste vide"
        return str(self.first_node)


if __name__ == "__main__":
    cl = ChainedList()
    cl.add_node(1)
    cl.add_node(3)
    cl.add_node(2)
    cl.add_node(4)
    cl.delete_node(2)
    print(a)
    print(b)

